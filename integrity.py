import sys
import os
import hashlib

file_path = sys.argv[1]
folder_path = sys.argv[2]

def call_check(word1=file_path, word2=folder_path):

    def compare_hash(file_name, hash_type, hash_sum):

        # specify path to folder with files under checksum validation
        full_path = folder_path + file_name
        hash_string = ''
        
        if hash_type == 'md5':
            hash_string = hashlib.md5(open(full_path, 'rb').read()).hexdigest()
        elif hash_type == 'sha1':
            hash_string = hashlib.sha1(open(full_path, 'rb').read()).hexdigest()
        elif hash_type == 'sha256':
            hash_string = hashlib.sha256(open(full_path, 'rb').read()).hexdigest()
        
        if hash_string == hash_sum:
            print(file_name + ' OK')
        elif hash_string != hash_sum:
            print(file_name + ' FAIL')

    line_list = [line.rstrip('\n') for line in open(file_path)]

    for file_line in line_list:
        string_list = file_line.split(" ")
        file_name = string_list[0]
        hash_type = string_list[1]
        hash_sum = string_list[2]
        try:
            compare_hash(file_name, hash_type, hash_sum)
        except OSError as e:
            print (file_name + ' NOT FOUND')

if __name__ == "__main__":
    call_check()